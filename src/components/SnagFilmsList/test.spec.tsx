/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import * as React from 'react';
import * as ShallowRenderer from 'react-test-renderer/shallow';

import { SnagFilmsListProps, SnagFilmsList } from './';


// Basic SnagFilmsList Test (loading):
it
(
    'SnagFilmsList renders correctly with a status of loading',
    () =>
    {
        let snagFilmsList: SnagFilmsListProps =
        {
            loading: true,
            films:[],
            error: null
        };

        let renderer = ShallowRenderer.createRenderer();

        renderer.render
        (
            <SnagFilmsList
              loading={snagFilmsList.loading}
              films={snagFilmsList.films}
              error={snagFilmsList.error}
            />
        );

        expect( renderer.getRenderOutput() ).toMatchSnapshot();
    }
);


// Basic SnagFilmsList Test (error):
it
(
    'SnagFilmsList renders correctly when an error occurs',
    () =>
    {
        let snagFilmsList: SnagFilmsListProps =
        {
            loading: false,
            films:[],
            error: { message:"A staged error has occured." }
        };

        let renderer = ShallowRenderer.createRenderer();

        renderer.render
        (
            <SnagFilmsList
              loading={snagFilmsList.loading}
              films={snagFilmsList.films}
              error={snagFilmsList.error}
            />
        );

        expect( renderer.getRenderOutput() ).toMatchSnapshot();
    }
);


// Basic SnagFilmsList Test (API Request):
it
(
    'SnagFilmsList renders correctly with content received from an API request',
    () =>
    {
        let snagFilmsList: SnagFilmsListProps =
        {
            loading: false,
            films:
                [
                    {id: "00000166-d0cd-d745-a9ef-d7ff560e0000", author: null, title: "Wizards 2019 Wizards Dog Calendar", images:{ image: [{ src: "src"}]}, type: null, logline: null },
                    {id: "00000166-bd76-dc91-a966-bffffbd10000", author: null, title: " Inside PressBox 10/28/18 Pt. 1: Towson Football Coach Rob Ambrose ", images:{ image: [{ src: "src"}]}, type: null, logline: null },
                    {id: "00000166-bd91-d745-a9ef-ffbf95aa0000", author: null, title: "Inside PressBox 10/28/18 Pt. 2: LifeBridge Health Sports Medicine Institute's Dr. Craig Bennett", images:{ image: [{ src: "src"}]}, type: null, logline: null },
                    {id: "00000166-bd95-dec1-abf7-ff9785fd0000", author: null, title: "Inside PressBox 10/28/18 Pt. 3: Cross Talk", images:{ image: [{ src: "src"}]}, type: null, logline: null },
                    {id: "00000166-bd98-dc91-a966-bfb98ed50000", author: null, title: "Inside PressBox 10/28/18 Pt. 4: Final Thoughts", images:{ image: [{ src: "src"}]}, type: null, logline: null },
                    {id: "00000166-c01d-d030-a76f-ecbfe49b0000", author: null, title: "Team Liquid Viewing Party", images:{ image: [{ src: "src"}]}, type: null, logline: null },
                    {id: "00000166-985e-d0d9-a96f-fbffbbd80000", author: null, title: "Inside PressBox 10/21/18 Pt. 1: Towson Basketball Coach Pat Skerry", images:{ image: [{ src: "src"}]}, type: null, logline: null },
                    {id: "00000166-9863-dc91-a966-9afba3a50000", author: null, title: "Inside PressBox 10/21/18 Pt. 2: Team Up For 1 Foundation", images:{ image: [{ src: "src"}]}, type: null, logline: null },
                    {id: "00000166-9868-dc91-a966-9af9ef2f0000", author: null, title: "Inside PressBox 10/21/18 Pt. 3: Luke Jackson", images:{ image: [{ src: "src"}]}, type: null, logline: null },
                    {id: "00000166-986b-d0d9-a96f-fbfff32c0000", author: null, title: "Inside PressBox 10/21/18 Pt. 4: Final Thoughts", images:{ image: [{ src: "src"}]}, type: null, logline: null }
                ],
            error: null
        };

        let renderer = ShallowRenderer.createRenderer();

        renderer.render(
            <SnagFilmsList
              loading={snagFilmsList.loading}
              films={snagFilmsList.films}
              error={snagFilmsList.error}
            />
        );

        expect( renderer.getRenderOutput() ).toMatchSnapshot();
    }
);
