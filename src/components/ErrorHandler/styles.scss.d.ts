export const errorHandlerContainer: string;
export const focusHeader: string;
export const titleCapture: string;
export const focusContent: string;
export const errorWrap: string;
export const errorMessage: string;
export const errorContentContainer: string;
