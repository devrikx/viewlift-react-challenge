/*------------------------------------------------------------------------------
 * @package:   SnagFilmsItem
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import * as React from 'react';
import * as ShallowRenderer from 'react-test-renderer/shallow';

import { SnagFilmsItemProps, SnagFilmsItem } from './';


// Basic SnagFilmsItem Test:
it
(
    'SnagFilmsItem renders correctly',
    () =>
    {
        let snagFilmsItem: SnagFilmsItemProps =
        {
            id: 1,
            title: "Test Post",
            image: 'image-url',
            runtime: 55
        };

        let renderer = ShallowRenderer.createRenderer();

        renderer.render
        (
            <SnagFilmsItem
             id={snagFilmsItem.id}
             title={snagFilmsItem.title}
             image={snagFilmsItem.image}
             runtime={snagFilmsItem.runtime}
            />
        );

        expect( renderer.getRenderOutput() ).toMatchSnapshot();
    }
);
