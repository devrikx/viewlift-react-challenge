/*------------------------------------------------------------------------------
 * @package:   viewlift-react-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


 // INCLUDES


 // DEFINES
 export type SnagFilmsItemImageType = {
    height: number,
    src: string;
    type:string;
    width: number;
    rokuSrc: string;
 }

 export type SnagFilmsItemType = {
    id: string,
    author: string,
    title: string,
    type: string,
    logline: string,
    description: string,
    year: string,
    addedDate: string,
    durationMinutes: number,
    durationSeconds: number,
    runtime: number;
    images: SnagFilmsItemImageType[];
    parentalRating: string;
    fbCommentsUrl: string;
    starRatingAvg: number,
    starRatingCount: number,
};

export type SnagFilmsListType = {
    films: SnagFilmsListType[];
}

export type SnagFilmsStateType =
{
    films: SnagFilmsListType;
    loading: boolean;
    error: boolean;
    invalidate: boolean;
    lastUpdated: number;
}