/*------------------------------------------------------------------------------
 * @package:   viewlift-react-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import * as React from 'react';
import * as ShallowRenderer from 'react-test-renderer/shallow';

import { UnderConstructionProps, UnderConstruction } from './';


// Basic UnderConstruction Test:
it
(
    'UnderConstruction renders correctly',
    () =>
    {
        let UnderConstructionProperties: UnderConstructionProps =
        {
        };

        let renderer = ShallowRenderer.createRenderer();

        renderer.render
        (
            <UnderConstruction/>
        );

        expect( renderer.getRenderOutput() ).toMatchSnapshot();
    }
);
