/*------------------------------------------------------------------------------
 * @package:   viewlift-react-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


 // INCLUDES
 import * as React from "react";
 import { connect } from "react-redux";
 import { fetchFilms } from "../../redux/actions";
 import { SnagFilmsList } from "../../components/SnagFilmsList";
 import { SlickSlider } from "../../library";


 // DEFINES
export interface SnagFilmsProps {
    films: any;
    loading: any,
    error: any;
};

interface StateFromProps {
    loading: boolean;
    error: any;
    films: {
        list: Array<any>;
        invalidate: boolean;
        lastUpdated: number;
    };
};

const mapStateToProps = ( state: any ) => ({
    films: state.snagFilms.films,
    loading: state.snagFilms.loading,
    error: state.snagFilms.error
});


/**
 * SnagFilms container component
 *
 * @since 0.1.0
 */
export class SnagFilmsContainer extends React.Component<SnagFilmsProps, any>
{
    /**
     * @var { string } displayName Always set the display name
     *
     * @since 0.1.0
     */
    displayName = "SnagFilmsContainer";


    /**
     * We'd set propTypes here if this wasn't Typescript.
     * We'd set default  props here if we wanted them.
     */


    /**
     * The slickSlider member provides us access to the SlickSliber library, as
     * we've designed it within this application. It wraps the `slick` library
     * and provides us an easy handle to initialization within any component that
     * makes use of `slick` in our application.
     *
     * @var { SlickSlider }
     */
    slickSlider: SlickSlider = null;


    /**
     * Constructor
     *
     * @param props
     *
     * @since 0.1.0
     */
    constructor( props?: SnagFilmsProps )
    {
        super( props );

        // The shape of our component's state:
        this.state = {
            loading: false,
            error: false,
            films: {
                lastUpdated: null,
                invalidate: true,
                list: []
            }
        };

        // Instantiate the SlickSlider library wrapper:
        this.slickSlider = new SlickSlider();
    }


    /**
     * ComponentDidMount fires after the component mounts
     *
     * @param { void }
     *
     * @return { void }
     *
     * @since 1.0.0
     */
    componentDidMount()
    {
        // We can implement a basic 'cache':
        const { films } = this.props;

        // For our basic example, we'll make the cache expire after 1 minute:
        if( films.invalidate || ( films.lastUpdated && ( Date.now() > ( films.lastUpdated + 60000 ) ) ) )
        {
            // At which point we'll launch a fetch request to grab a fresh
            // API response:
            ( this.props as any ).dispatch( fetchFilms() );
        }

        // Otherwise our state already has a recent enough list of films.
    }


    /**
     * This method fires whena component updates - or re-renders
     *
     * @param { void }
     *
     * @return { void }
     *
     * @since 1.0.0
     */
    componentDidUpdate()
    {
        // Prepare a configuration for the SlickSlider initialization method:
        const slickSliderSettings = {
            dots: true,
            arrows: false,
            infinite: true,
            autoplay: false
            /*,
            responsive:
            [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
            */
        };

        // Initialize the SlickSlider showcase widget:
        this.slickSlider.render( 'slickity', slickSliderSettings );
    }


    /**
     * Container components pass rendering responsibilities off to
     * presentational components
     *
     * @param { void }
     *
     * @since 0.1.0
     */
    render()
    {
        const { error, loading, films } = this.props;

        // A simple way to handle API errors:
        if( error )
        {
            console.log( 'Rendering error' );
            return  <div>[Error]: {error.message}</div>
        }

        // A simple way to diplay a loading status:
        if( loading || !films || !films.list || !films.list.film )
        {
            console.log( 'Rendering loading' );
            return <div>Loading...</div>
        }

        // When a successful API response is received:
        return (
            <SnagFilmsList
              loading={loading}
              films={films.list.film}
              error={error}
            />
        );
    }
}


// Here we wrap the SnagFilmsCotnainer with a Connect container,
// creating the SnagFilms Container:
export default connect<StateFromProps>(
    mapStateToProps
)( SnagFilmsContainer )
