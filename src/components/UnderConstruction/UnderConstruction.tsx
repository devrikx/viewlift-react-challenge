/*------------------------------------------------------------------------------
 * @package:   viewlift-react-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import { styles } from "./";
import * as React from "react";


// DEFINES
export type UnderConstructionProps = {
};


/**
 * UnderConstruction presentational component
 *
 * @param { UnderConstructionProps } props
 *
 * @since 0.1.0
 */
export const UnderConstruction: React.SFC<UnderConstructionProps> = ( props: any ) =>
{
    // Here we render the component:
    return(
        <div className={styles.underConstructionContainer}>

            <div className={"row " + styles.focusHeader}>
                <div className="col-md-12">
                    <h1><span className={styles.titleCapture}>ViewliftChallengeApp</span> is Under Construction</h1>
                </div>
            </div>

            <div className={styles.focusContent}>
                <div className={styles.ucWrap + " " + styles.ucMessage}>
                    <h1>Whoa!?</h1>
                    <small>We apologize for any inconvenience. Check back often for updates, or <a href="mailto:support@example.com">Email Support</a> with any questions or concerns.</small>
                </div>
            </div>
        </div>
    );
}


/**
 * @var { string } displayName Always set the display name
 *
 * @since 0.1.0
 */
UnderConstruction.displayName = "UnderConstruction";


/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted them.
 */
