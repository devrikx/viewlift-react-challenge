/*------------------------------------------------------------------------------
 * @package:   viewlift-react-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import { styles } from "./";
import * as React from "react";
import { SnagFilmsItem } from "../SnagFilmsItem";



// DEFINES
export type SnagFilmsListProps = {
    loading: boolean;
    films: any;
    error: any;
};


/**
 * SnagFilmsList presentation component
 *
 * @param { SnagFilmsListProps } props
 *
 * @since 0.1.0
 */
export const SnagFilmsList: React.SFC<SnagFilmsListProps> = ( props ) =>
{
    // Some control:
    let classes: string = "",
        filmList: any;

    // Prepare a fallback for loading - or for an empty response::
    if( props.loading || !props.films || !props.films.length )
    {
        classes += " " + styles.noFilms;
    }

    // Prep a list of films from SnagFilms API (if any exist):
    classes += styles.filmList,
    filmList = ( props.films && props.films.length ) ? props.films.map(
        ( film: any ) => (
            <SnagFilmsItem
              id={film.id}
              title={film.title}
              image={film.images.image[0].src}
              runtime={film.durationMinutes}
            />
        )
    ): '';

    // Here we render the component:
    return(
        <div className={classes}>
            <div className={"slickity " + styles.filmSlider}>
                {filmList}
            </div>
        </div>
    );
}


/**
 * @var { string } displayName Always set the display name
 *
 * @since 0.1.0
 */
SnagFilmsList.displayName = "SnagFilmsList";


/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted them.
 */
