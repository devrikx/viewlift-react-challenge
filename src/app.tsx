/*------------------------------------------------------------------------------
 * @package:   viewlift-react-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import "@babel/polyfill";
import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import store from "./redux/store/index";
import { App } from "./containers/App";


// DEFINES

/* To help debug state in a browser:
( window as any ).store = store;
*/



// Render the application:
ReactDOM.render
(
    // Render our application's root component, wrapped with the react-redux provider component:
    <Provider store={store}>
        <App
          title='ViewliftChallengeApp'
        />
    </Provider>,
    document.getElementById( "viewlift-app" )
);
