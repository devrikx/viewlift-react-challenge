/*------------------------------------------------------------------------------
 * @package:   viewlift-react-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import { styles } from "./";
import * as React from "react";
import { NavLink } from "react-router-dom";


// DEFINES
export type SnagFilmsItemProps = {
    id: number;
    title: string;
    image: string;
    runtime: number;
};


/**
 * SnagFilms Film Item presentation component
 *
 * @param { SnagFilmsItemProps } props
 *
 * @since 0.1.0
 */
export const SnagFilmsItem: React.SFC<SnagFilmsItemProps> = ( props ) =>
{
    // Here we render the component:
    return(
        <div key={props.id} className={styles.filmItem}>
            <div className={styles.filmItemInner}>
                <div className={styles.coverImage}>
                    <img src={props.image + "?impolicy=resize&w=233&h=345"}/>
                </div>
                <div className={styles.coverImageMask}></div>
                <div className={styles.coverDetails}>
                    <span className={styles.coverDetailsTitle}>{props.title}</span>
                    <span className={styles.coverDetailsInfo}>{props.runtime + "MINS"}</span>
                    <a href="#" className={"btn btn-primary " + styles.coverDetailsActionButton}>WATCH NOW</a>
                </div>
            </div>
        </div>
    );
}


/**
 * @var { string } displayName Always set the display name
 *
 * @since 0.1.0
 */
SnagFilmsItem.displayName = "SnagFilmsItem";


/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted them.
 */
