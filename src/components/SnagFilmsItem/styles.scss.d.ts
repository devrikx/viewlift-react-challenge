export const filmItem: string;
export const filmItemInner: string;
export const coverImage: string;
export const coverImageMask: string;
export const coverDetails: string;
export const coverDetailsTitle: string;
export const coverDetailsInfo: string;
export const coverDetailsActionButton: string;
