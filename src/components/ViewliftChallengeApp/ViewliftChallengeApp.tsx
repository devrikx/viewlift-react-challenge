/*------------------------------------------------------------------------------
 * @package:   viewlift-react-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import { styles } from "./";
import * as React from "react";
import { Switch, Route, HashRouter } from "react-router-dom";
import SnagFilms from "../../containers/SnagFilms";
import { ErrorHandler } from "../../components/ErrorHandler";
//import { UnderConstruction } from "../../components/UnderConstruction";


// DEFINES
export interface ViewliftChallengeAppProps { title?: string; }


/**
 * ViewliftChallengeApp component
 *
 * @param { ViewliftChallengeAppProps } props
 *
 * @since 0.1.0
 */
export const ViewliftChallengeApp: React.SFC<ViewliftChallengeAppProps> = ( props ) =>
{
    // Here we render the component:
    return (
        <HashRouter>
            <div className={styles.viewliftAppContainerInner}>
                <div className={"row " + styles.contentBlock}>
                    <div className={"col-md-12 "  + styles.contentBlockInner}>
                        <Switch>
                            <Route exact path="/" component={SnagFilms}/>
                            <Route component={ErrorHandler} />
                        </Switch>
                    </div>
                </div>
            </div>
        </HashRouter>
    );
}


/**
 * @var { string } displayName Always set the display name
 *
 * @since 0.1.0
 */
ViewliftChallengeApp.displayName = "ViewliftChallengeApp";


/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted to.
 */
