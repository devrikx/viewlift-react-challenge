/*------------------------------------------------------------------------------
 * @package:   viewlift-react-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import {
    FETCH_FILMS_BEGIN,
    FETCH_FILMS_SUCCESS,
    FETCH_FILMS_ERROR
} from "../constants/action-types";


// DEFINES
const initialState = {
    loading: false,
    error: <any>null,
    films: {
        invalidate: true,
        lastUpdated: <any>null,
        list: <any>[]
    }
};


// Create a reducer:
const snagFilms = ( state = initialState, action: any ) =>
{
    console.log( 'Action.type = ' )
    console.log( action.type );
    switch( action.type )
    {
        case FETCH_FILMS_BEGIN:
        {
            console.log( 'Reducer fetch-films-begin triggered' );
            // The loading state. Render a loader and wipe errors:
            return { ...state, loading: true, error: null };
        }break;

        case FETCH_FILMS_SUCCESS:
        {
            console.log( 'Reducer fetch-films-success triggered' );
            console.log( 'Payload:' );
            console.log( action.payload.films );
            // The success state. We have the API response, render it:.
            return { ...state, loading: false, films: { list: action.payload.films , invalidate: false, lastUpdated: Date.now() } };
        }break;

        case FETCH_FILMS_ERROR:
        {
            console.log( 'Reducer fetch-films-error triggered' );
            // The error state. Render the error in our simple challenge app:
            return { ...state, loading: false, error: action.payload.error, films: { list: [], invalidate: false, lastUpdated: null } };
        }break;

        default:
        {
            console.log( 'default reducer action case triggered');
            return state;
        }break;
    }
};


// Export it:
export default snagFilms;
